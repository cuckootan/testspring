package com.netease.jdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by cuckootan on 17-3-15.
 */
public class TestJdbc extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.getWriter().println("test jdbc");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext servletContext = this.getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        // 使用 JdbcTemplate，此时 sql 语句中要使用 ?
/*        JdbcTemplateDao dao = context.getBean("jdbcTemplateDao", JdbcTemplateDao.class);
        dao.createTable();
        dao.insertData();
        System.out.println(dao.count());
        List<User> userList = dao.getUserList();
        for (User user: userList)
        {
            System.out.println(user.getId() + ": " + user.getFirstName() + " " + user.getLastName());
        }*/

        // 使用 NamedParameterJdbcTemplate，此时 sql 语句中不必使用 ?
        NamedParameterJdbcTemplateDao dao = context.getBean("namedParameterJdbcTemplateDao", NamedParameterJdbcTemplateDao.class);
        User exampleUser = new User();
        exampleUser.setFirstName("San");
        System.out.println(dao.countOffUsersByFirstName(exampleUser));

        ((AbstractApplicationContext)context).close();

        super.init();
    }
}
