package com.netease.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

/**
 * Created by cuckootan on 17-3-15.
 */
@Repository
public class NamedParameterJdbcTemplateDao
{
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public int countOffUsersByFirstName(User exampleUser)
    {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(exampleUser);
        String sql = "SELECT COUNT(*) FROM users2 WHERE firstName = :firstName";

        return this.namedParameterJdbcTemplate.queryForObject(sql, namedParameters, Integer.class);
    }
}
