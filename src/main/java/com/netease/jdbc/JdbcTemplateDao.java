package com.netease.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by cuckootan on 17-3-15.
 */
// 定义一个 Dao，并定义相应的 bean
@Repository
public class JdbcTemplateDao
{
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void createTable()
    {
        jdbcTemplate.execute("CREATE TABLE users2 (id INTEGER, firstName VARCHAR(20), lastName VARCHAR(20))");
    }

    public void insertData()
    {
        this.jdbcTemplate.update("INSERT INTO users2 VALUES (1, ?, ?)", "San", "Zhang");
        this.jdbcTemplate.update("INSERT INTO users2 VALUES (2, ?, ?)", "Si", "Li");
    }

    public int count()
    {
        return this.jdbcTemplate.queryForObject("SELECT COUNT(*) FROM users2", Integer.class);
    }

    public List<User> getUserList()
    {
        // Lambda 表达式
        return this.jdbcTemplate.query("SELECT * FROM users2", (ResultSet rs, int rowNum) ->
        {
            User user = new User();

            user.setId(rs.getInt("id"));
            user.setFirstName(rs.getString("firstName"));
            user.setLastName(rs.getString("lastName"));

            return user;
        });
    }
}
