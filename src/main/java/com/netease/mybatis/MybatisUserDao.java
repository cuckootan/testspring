package com.netease.mybatis;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by cuckootan on 17-3-15.
 */
public interface MybatisUserDao
{
    // 当返回的是一个 User 对象时，如果数据库中的列名与实例中相应的属性名不同，则需要用 @Result 来进行映射（若返回的是其中某个字段，则无需如此）
    /*
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "lastName", column = "lastName")
    })
    */

    // 更为方便地是建立一个 UserMapper.xml 文件，在其中添加这些映射，然后在 ApplicationContextMybatis.xml 中通过 mapperLocations 属性进行加载。使用时只需要添加标注：
    // @ResultMap(result namespace.resultMapId)，例如：@ResultMap("com.netease.mybatis.MybatisUserDao.UserResult")
    // 其中所指定的是对应 resultMap 的 id。形式为：命名空间.id

    @ResultMap("com.netease.mybatis.MybatisUserDao.UserResult")
    @Select("SELECT * FROM users2 WHERE firstName = #{firstName}")
    // 还可以设置其他选项： @Options(useGeneratedKeys = true, keyProperty = "id")
    User getUser(@Param("firstName") String firstName);

    // 如果数据库中的列名与实例中相应的属性名不同，则需要用 @Result 来进行映射。同上
    // 虽然这里是返回一个 User 的 List，不过仍然只需在 UserMapper.xml 文件中定义 type 为 com.netease.mybatis.User 即可。
    // 如果要返回 List<Map<>>，则需要设置 @ResultType(java.util.Map.class)
    @Select("SELECT * FROM users2")
    List<User> getUserList();
}
