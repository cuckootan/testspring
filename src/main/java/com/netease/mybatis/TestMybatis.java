package com.netease.mybatis;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by cuckootan on 17-3-15.
 */
public class TestMybatis extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.getWriter().println("test mybatis");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext servletContext = this.getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        MybatisUserDao dao = context.getBean("mybatisUserDao", MybatisUserDao.class);
        List<User> userList = dao.getUserList();
        for (User user: userList)
        {
            System.out.println(user.getFirstName() + " " + user.getLastName());
        }

        User user = dao.getUser("San");
        System.out.println(user.getFirstName() + " " + user.getLastName());

        ((AbstractApplicationContext) context).close();

        super.init();
    }
}
