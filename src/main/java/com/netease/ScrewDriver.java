package com.netease;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by cuckootan on 17-3-13.
 */
public class ScrewDriver
{
    private String color = "red";

    public void use()
    {
        System.out.println("Use " + color + " screwDriver");
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    // 除了在 ApplicationContext.xml　中指定之外，还可以通过注解的方式进行指定。
    // @PostConstruct
    public void init()
    {
        System.out.println("init something after constructing object");
    }

    // 除了在 ApplicationContext.xml　中指定之外，还可以通过注解的方式进行指定。
    // @PreDestroy
    public void destroy()
    {
        System.out.println("destroy something before destroying object");
    }
}
