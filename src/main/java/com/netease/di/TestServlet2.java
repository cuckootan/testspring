package com.netease.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cuckootan on 17-3-14.
 */
public class TestServlet2 extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.getWriter().println("test di with setter");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext servletContext = this.getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        //　通过 setter 将 color 和 size 注入到 straightHeader2 中。
        Header header = context.getBean("straightHeader-setter", StraightHeader2.class);
        System.out.println(header.getInfo());
        header.doWork();

        // 通过 setter 将 header（是一个 bean）注入到　screwDriver2　中
        ScrewDriver2 screwDriver = context.getBean("screwDriver-setter", ScrewDriver2.class);
        screwDriver.use();

        ((AbstractApplicationContext) context).close();

        super.init();
    }
}
