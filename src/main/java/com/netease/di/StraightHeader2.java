package com.netease.di;

/**
 * Created by cuckootan on 17-3-14.
 */
public class StraightHeader2 implements Header
{
    private String color;
    private int size;

    public void setColor(String color)
    {
        this.color = color;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    @Override
    public void doWork()
    {
        System.out.println("Do work with straight header");
    }

    @Override
    public String getInfo()
    {
        return "StraightHeader: color = " + color + ", size = " + size;
    }
}
