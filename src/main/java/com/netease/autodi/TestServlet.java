package com.netease.autodi;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cuckootan on 14/03/2017.
 */
public class TestServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.getWriter().println("test annotation autodi");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext servletContext = this.getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        //　通过构造器将 color 和 size 注入到 straightHeader 中。
        Header header = context.getBean("header", StraightHeader.class);
        System.out.println(header.getInfo());
        header.doWork();

        // 通过构造器将 header（是一个 bean）注入到　screwDriver　中
        ScrewDriver screwDriver = context.getBean("screwDriver", ScrewDriver.class);
        screwDriver.use();

        ((AbstractApplicationContext) context).close();

        super.init();
    }
}
