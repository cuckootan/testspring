package com.netease.autodi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by cuckootan on 17-3-14.
 */
// 定义为 bean，括号里的是 bean 的 id。此时就可以将 ApplicationContext.xml 中相应的 bean 去掉
@Component("header")
public class StraightHeader implements Header
{
    // 定义 property，此时就可以将 setter 方法去掉。它相当于是配置文件中对应 bean 的 property 中的 value
    @Value("red")
    private String color;
    @Value("3")
    private int size;

    @Override
    public void doWork()
    {
        System.out.println("Do work with straight header");
    }

    @Override
    public String getInfo()
    {
        return "StraightHeader: color = " + color + ", size = " + size;
    }
}
