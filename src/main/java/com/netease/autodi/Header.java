package com.netease.autodi;

/**
 * Created by cuckootan on 17-3-14.
 */
public interface Header
{
    void doWork();
    String getInfo();
}
