package com.netease.autodi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by cuckootan on 17-3-14.
 */
@Component("screwDriver")
public class ScrewDriver
{
    // 按类型查找出所需要的 bean。
    @Autowired
    private Header header;

    public void use()
    {
        System.out.println("Use header: " + header.getInfo());
        header.doWork();
    }
}
