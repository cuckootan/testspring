package com.netease;

import javafx.application.Application;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cuckootan on 17-3-13.
 */
public class TestScrewDriverServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.getWriter().println("test container");
    }

    @Override
    public void init() throws ServletException
    {
        // 如果不是通过 web.xml 文件中进行配置并初始化 application context，则可以使用如下语句进行初始化
        // ApplicationContext context = new ClassPathXmlApplicationContext("spring/ApplicationContext.xml");

        ServletContext servletContext = this.getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        // getBean 的第一个参数是　bean　的 id
        ScrewDriver screwDriver = context.getBean("screwDriver", ScrewDriver.class);
        screwDriver.setColor("green");
        screwDriver.use();

        // singleton　下，screwDriver1　与　screwDriver　所指向的是同一个对象。
        // ScrewDriver screwDriver1 = context.getBean("screwDriver", ScrewDriver.class);
        // green
        // screwDriver1.use();

        // prototype　下，每次通过　getBean　方法都会得到一个新的对象。
        ScrewDriver screwDriver1 = context.getBean("screwDriver", ScrewDriver.class);
        // red
        screwDriver1.use();

        // 由于 ApplicationContext　对象没有 close 方法，而 ConfigurableApplicationContext　或者 AbstractApplicationContext　有，因此需要先进行转换然后调用　close

        ((AbstractApplicationContext) context).close();

        super.init();
    }
}
