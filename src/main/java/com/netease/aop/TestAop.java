package com.netease.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cuckootan on 17-3-15.
 */
public class TestAop extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.getWriter().println("test aop");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext servletContext = this.getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        Caculator caculator = context.getBean("caculator", Caculator.class);
        System.out.println(caculator.add(1, 1));
        System.out.println(caculator.sub(5, 2));

        ((AbstractApplicationContext)context).close();

        super.init();
    }
}
