package com.netease.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * Created by cuckootan on 17-3-15.
 */
// 将这个类定义为一个 aspect，但是并不会将其定义为一个 bean
@Aspect
public class LoggingAspect
{
    // 定义 pointcut 以及 advice
    // 只要试图调用 Caculator 中的任何方法，都会捕获传入到该方法的第一个参数 a，然后自动调用下面这个方法。调用完成后再继续调用 Caculator 中的该方法
    @Before("execution(* com.netease.aop.Caculator.*(..)) && args(a, ..)")
    private void arithmeticDoLog(JoinPoint jp, int a)
    {
        System.out.println(a + ": " + jp.toString());
    }
}
